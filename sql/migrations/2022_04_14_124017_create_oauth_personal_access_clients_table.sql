--
-- Migration: create_oauth_personal_access_clients_table
--

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- Create oauth_personal_access_clients table
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

  -- Create `oauth_personal_access_clients` table
  CREATE TABLE `oauth_personal_access_clients` (

    -- Fields
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `client_id` bigint(20) unsigned NOT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,

    -- Keys
    PRIMARY KEY (`id`)

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

  -- Drop oauth_personal_access_clients table
  DROP TABLE `oauth_personal_access_clients`;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;