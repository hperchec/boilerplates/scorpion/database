--
-- Migration: create_users_table
--

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- Create users table
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

  -- Create `users` table
  CREATE TABLE `users` (

    -- Fields
    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
    `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Password encrypted with bcrypt',
    `firstname` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Firstname',
    `lastname` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Lastname',
    `birth_date` date DEFAULT NULL COMMENT 'Birth date',
    `thumbnail` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'User thumbnail (file name)',
    `role` enum('admin','user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user' COMMENT 'User role (''admin'' or ''user'')',
    `email_verified_at` timestamp NULL DEFAULT NULL,
    `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL,

    -- Keys
    PRIMARY KEY (`id`),
    UNIQUE KEY `users_email_unique` (`email`)

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

  -- Drop users table
  DROP TABLE `users`;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;