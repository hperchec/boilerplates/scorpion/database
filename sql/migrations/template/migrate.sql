--
-- Use database
--
USE __DATABASE_NAME__;

--
-- Set dynamically procedure to call (UP or DOWN)
--
SET @PROC = '__PROC__';

--
-- Auto delete UP and DOWN procedure
--
DROP PROCEDURE IF EXISTS UP;
DROP PROCEDURE IF EXISTS DOWN;

--
-- MIGRATION CONTENT WILL BE INSERTED HERE
--
__MIGRATION_CONTENT__

--
-- Auto delete UP and DOWN procedure
--
DROP PROCEDURE UP;
DROP PROCEDURE DOWN;
