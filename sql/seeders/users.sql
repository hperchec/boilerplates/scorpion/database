/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Seed `users` table
--
INSERT INTO `users` (
    `id`,
    `email`,
    `password`,
    `firstname`,
    `lastname`,
    `birth_date`,
    `thumbnail`,
    `role`,
    `email_verified_at`,
    `remember_token`,
    `created_at`,
    `updated_at`
) VALUES
(
    1,
    "admin@app.com",
    "$2y$10$VpH0QUfK3ZYlzfBaLEF8MO6gjv83smGalXVEdW/P9SxbiekoQks9.", -- 'test'
    "Admin",
    "Admin",
    NULL,
    NULL,
    "admin",
    "2022-02-02 14:23:00",
    NULL,
    "2022-04-01 19:00:44",
    "2022-04-01 19:00:44"
),
(
    2,
    "test@app.com",
    "$2y$10$VpH0QUfK3ZYlzfBaLEF8MO6gjv83smGalXVEdW/P9SxbiekoQks9.", -- 'test'
    "John",
    "Doe",
    NULL,
    NULL,
    "user",
    "2022-02-02 14:23:00",
    NULL,
    "2022-04-01 19:00:44",
    "2022-04-01 19:00:44"
);

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;