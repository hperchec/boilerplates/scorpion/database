-------------------------------------------------------------
-- ! THIS COMMENT BLOCK WILL BE REMOVED !
-- SEEDER TEMPLATE
-- @author Hervé Perchec <herve.perchec@gmail.com>
-------------------------------------------------------------
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Seed `__TABLE_NAME__` table
--
INSERT INTO `__TABLE_NAME__` (
    __TABLE_FIELDS__
) 
VALUES
('<value_1>', '<value_2>', ...);

/*!40101 SET character_set_client = @saved_cs_client */;