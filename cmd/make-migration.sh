#!/bin/bash

# --------------------------------------------------------------
# Create new migration script
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------

# Get entire path of the script directory
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR
SCRIPT_DIR=$PWD

# Get sql directory path
SQL_DIR=$SCRIPT_DIR/../sql

# Defines migration template path
MIGRATION_TEMPLATE_PATH=$SQL_DIR/migrations/template/template.sql

# Init MIGRATION_NAME to empty string
MIGRATION_NAME=""
# Init TABLE_NAME to empty string
TABLE_NAME=""

# Init MIGRATION_FILE_NAME to empty string
MIGRATION_FILE_NAME=""

# Defines the verbosity level
# Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH', default: 'HIGH'
VERBOSE_LEVEL=HIGH

# Init exit code, default: 0 (Success)
EXIT_CODE=0

# Show header function
show_header() {
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo -e "\e[36mCreate new migration script\e[0m"
    echo -e "\e[36m@author Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo
}

# Show help function
show_help() {
    echo -e "Usage: make-migration.sh \e[36m<name>\e[0m \e[33m[OPTIONS]\e[0m"
    echo
    echo -e "\e[36mARGS:\e[0m"
    echo
    echo -e "  \e[36m<name>\e[0m             Migration name (it will be auto-prefixed by date)."
    echo
    echo -e "\e[33mOPTIONS:\e[0m"
    echo
    echo -e "  \e[33m-h\e[0m                 Show this help."
    echo -e "  \e[33m-t\e[0m                 Name of the target table."
    echo -e "  \e[33m-v\e[0m                 Defines the verbose level. Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH' (Default: 'HIGH')."
    echo
}

# Quit function
quit() {
    exit $EXIT_CODE
}

# --------------------------------------------------------------
# END INIT
# --------------------------------------------------------------

# Loop on passed arguments
#
# Solution found on stackoverflow to not give matter of OPTIONS/ARG position
# It can be :
#   - make-migration.sh OPTIONS ARG
#   - make-migration.sh ARG OPTIONS
#   - make-migration.sh OPTION ARG OPTION
#   - etc...
# https://stackoverflow.com/questions/11742996/is-mixing-getopts-with-positional-parameters-possible
while [ $# -gt 0 ]
do
    unset OPTIND
    unset OPTARG
    # Get options
    # -h : To show command help
    # -t : Name of the target table
    # -v : To define verbose level
    while getopts "ht:v:" opt
    do
        case $opt in
            v) 
                case $OPTARG in
                    "NONE") VERBOSE_LEVEL=${OPTARG} ;;
                    "LOW") VERBOSE_LEVEL=${OPTARG} ;;
                    "MEDIUM") VERBOSE_LEVEL=${OPTARG} ;;
                    "HIGH") VERBOSE_LEVEL=${OPTARG} ;;
                    *) 
                        show_header
                        echo -e "\e[31mInvalid value for -v option\e[0m"
                        echo
                        show_help
                        EXIT_CODE=1 # Exit code: 1 (Error)
                        quit
                        ;;
                esac
                ;;
            t)
                TABLE_NAME=${OPTARG}
                ;;
            h) 
                show_header
                show_help
                quit
                ;;
            *)
                show_header
                show_help
                EXIT_CODE=1 # Exit code: 1 (Error)
                quit
                ;;
        esac
    done
    shift $((OPTIND-1))
    ARGS="${ARGS:+${ARGS} }${1}"
    shift
done

# Build array of args
ARGS=($(echo $ARGS))
# Get first arg -> migration name
MIGRATION_NAME=${ARGS[0]}

# --------------------------------------------------------------
# END SCRIPT OPTIONS PARSING
# --------------------------------------------------------------

# MIGRATION_NAME cannot be empty
if [ -z ${MIGRATION_NAME} ]; then
    show_header
    echo -e "\e[31mYou need to specify a migration name\e[0m"
    echo
    show_help
    EXIT_CODE=1 # Exit code: 1 (Error)
    quit
fi

# Cleans MIGRATION_NAME (trim spaces)
MIGRATION_NAME=$(echo $MIGRATION_NAME)

# --------------------------------------------------------------
# END SCRIPT ARGS PARSING
# --------------------------------------------------------------

# Call show_header
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    show_header
fi

# --------------------------------------------------------------
# MAIN FUNCTION
# --------------------------------------------------------------

main() {

    #
    # 1 - Create migration file based on the template
    #

    # Get current date
    CURRENT_DATE=$(date +'%Y_%m_%d_%H%M%S')
    # Build file name
    MIGRATION_FILE_NAME=$CURRENT_DATE\_$MIGRATION_NAME.sql
    # Defines full path
    MIGRATION_FILE_PATH=$SQL_DIR/migrations/$MIGRATION_FILE_NAME
    # Copy template to new file in migrations folder
    cp $MIGRATION_TEMPLATE_PATH $MIGRATION_FILE_PATH

    # If last command exit code is error -> abort
    if [ $? -ne 0 ]; then
        EXIT_CODE=1
        quit
    fi

    #
    # 2 - Replace values in the new file
    #

    # Remove the the five first line of the new file (Template comment)
    sed -i '1,5d' $MIGRATION_FILE_PATH
    # Replace __MIGRATION_NAME__
    sed -i 's/__MIGRATION_NAME__/'"$MIGRATION_NAME"'/' $MIGRATION_FILE_PATH
    # Replace __TABLE_NAME__
    sed -i 's/__TABLE_NAME__/'"$TABLE_NAME"'/' $MIGRATION_FILE_PATH

    #
    # End - Success message
    #

    if [ $VERBOSE_LEVEL != "NONE" ]; then
        echo -e "\e[32mNew migration '$MIGRATION_NAME' successfully created ✔\e[0m"
        echo
        echo -e "- $(realpath $MIGRATION_FILE_PATH)"
        echo
    fi

}

# --------------------------------------------------------------

# Call main()
main

# Exit with success
quit