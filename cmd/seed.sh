#!/bin/bash

# --------------------------------------------------------------
# Seed tables script
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------

# Get entire path of the script directory
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR
SCRIPT_DIR=$PWD

# Get sql directory path
SQL_DIR=$SCRIPT_DIR/../sql

# Get config (DATABASE_NAME var)
source ../config

# Get sql/seeders directory path
SEEDERS_DIR=$SQL_DIR/seeders

# Defines the verbosity level
# Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH', default: 'HIGH'
VERBOSE_LEVEL=HIGH

# Init exit code, default: 0 (Success)
EXIT_CODE=0

# Show header function
show_header() {
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo -e "\e[36mSeed tables script\e[0m"
    echo -e "\e[36m@author Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo
}

# Show help function
show_help() {
    echo -e "Usage: seed.sh \e[33m[OPTIONS]\e[0m"
    echo
    echo -e "\e[33mOPTIONS:\e[0m"
    echo
    echo -e "  \e[33m-h\e[0m         Show this help."
    echo -e "  \e[33m-v\e[0m         Defines the verbose level. Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH' (Default: 'HIGH')."
    echo
}

# Quit function
quit() {
    exit $EXIT_CODE
}

# --------------------------------------------------------------
# END INIT
# --------------------------------------------------------------

# Loop on passed arguments
#
# Solution found on stackoverflow to not give matter of OPTIONS/ARG position
# It can be :
#   - seed.sh OPTIONS ARG
#   - seed.sh ARG OPTIONS
#   - seed.sh OPTION ARG OPTION
#   - etc...
# https://stackoverflow.com/questions/11742996/is-mixing-getopts-with-positional-parameters-possible
while [ $# -gt 0 ]
do
    unset OPTIND
    unset OPTARG
    # Get options
    # -h : To show command help
    # -v : To define verbose level
    while getopts "hv:" opt
    do
        case $opt in
            v) 
                case $OPTARG in
                    "NONE") VERBOSE_LEVEL=${OPTARG} ;;
                    "LOW") VERBOSE_LEVEL=${OPTARG} ;;
                    "MEDIUM") VERBOSE_LEVEL=${OPTARG} ;;
                    "HIGH") VERBOSE_LEVEL=${OPTARG} ;;
                    *) 
                        show_header
                        echo -e "\e[31mInvalid value for -v option\e[0m"
                        echo
                        show_help
                        EXIT_CODE=1 # Exit code: 1 (Error)
                        quit
                        ;;
                esac
                ;;
            h) 
                show_help
                quit
                ;;
            *)
                echo
                show_help
                EXIT_CODE=1 # Exit code: 1 (Error)
                quit
                ;;
        esac
    done
    shift $((OPTIND-1))
    ARGS="${ARGS:+${ARGS} }${1}"
    shift
done

# Build array of args
ARGS=($(echo $ARGS))
# Get first arg -> target migration name (optional)
TARGET_SEEDER=${ARGS[0]}

# --------------------------------------------------------------
# END SCRIPT OPTIONS PARSING
# --------------------------------------------------------------

# Call show_header
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    show_header
fi

# --------------------------------------------------------------
# MAIN FUNCTION
# --------------------------------------------------------------

main() {

    #
    # 1 - Loop on sql files to seed tables
    #

    # Defines if target seeder is found
    TARGET_SEEDER_FOUND=0 # False by default

    for filepath in $SEEDERS_DIR/*.sql; do

        filename=$(basename $filepath)
        table_name=$(basename $filepath .sql) # Remove ".sql" from file name

        # Check if TARGET_SEEDER is defined
        if [[ ! -z ${TARGET_SEEDER} ]]; then
            if [ $TARGET_SEEDER == $table_name ]; then
                TARGET_SEEDER_FOUND=1
            else
                continue
            fi
        fi

        if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
            echo -ne "- Seed table \e[36m'$table_name'\e[0m: "
        fi
        
        SEEDER_CONTENT=$(cat "$SEEDERS_DIR/$filename")

        # Run sql script
        mysql -D $DATABASE_NAME -e ''"$SEEDER_CONTENT"'' > /dev/null 2>&1 # Don't show any output

        # If mysql cmd exit code is error -> abort
        if [ $? -ne 0 ]; then
            if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
                echo -e "\e[31mFailed\e[0m"
            fi
            mysql -D $DATABASE_NAME -e ''"$SEEDER_CONTENT"''
            echo
            EXIT_CODE=1
            quit
        else
            if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
                echo -e "\e[32mSuccess\e[0m"
            fi
        fi

    done

    # If TARGET_SEEDER is defined
    if [[ ! -z ${TARGET_SEEDER} ]]; then
        # If not found -> return error
        if [ $TARGET_SEEDER_FOUND -eq 0 ]; then
            echo -e "\e[31mERROR: Seeder '$TARGET_SEEDER' does not exist...\e[0m"
            echo
            EXIT_CODE=1
            quit
        fi
    fi

    #
    # End - Success message
    #

    if [ $VERBOSE_LEVEL != "NONE" ]; then
        echo
        echo -e "\e[32mSeed tables script successfully executed.\e[0m"
        echo
    fi

}

# --------------------------------------------------------------

# Call main()
main

# Exit with success
quit