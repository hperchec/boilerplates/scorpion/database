# App (Database)

[![app](https://img.shields.io/static/v1?label=&message=Scorpion&color=362F2D&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACWklEQVQ4T4WSTUhUURTH753PN2MTRZAtAinKdiGt2ojbCAIrmEghU4i+aMApyU3ja2aYmaaNOBLRQl0UUUESgSESDhTkJiRKTFv0gVEkBco0737MPaf7RuerUu/q8e45v3v+//+hpOoszya2+Ti5TySZcS1u6qWHQ7z6/n/ftPSTzSd3OyUdIxz2EQaEcJyGnGrzHjHfrwcpAwqzybsoSftKM8wgUxOEkS5lFZp8wfjHtSAVwLtEBwoyYgOQawiDTuDwkwhsMIKxwQ0BOGVuLjjcP/TrXrSnYAgoVMrz1tlHTbOwIcAukK/i87p5r262ZRAbRBlmJYe2urOJb+uaWAS8iH1HhvV2sy0FGC5QrqaIBQe1nNO+y+nnf0PKHtgXIhvjutFT9KEkg5NG+lueQIlRtFTUIIG4lqRfWDllAI7frJNOKwcWXHJwyKyOn3crdwP/tbSFKNeHIpTjhMFkO81kFmsBk+ZOyelrz6G+evEgcpEwdZwKfOk+k4jYhez6lWW0IGBPRwV5Ytzqb60B5J6Z+z2KvEEBQe+x6KNqrcwMN6Kic9qLojc62mHfnYGuGoAcu9aC0pnVC/QVODb7TlWWJ2f27HBx+KwlfNE+7NEmX/UPD6ZrAPyp2UoFjK6aJwhXEe+F1I3SJFZPdwvmIUwENFPpPOAb6f9UAxCPI53a6aHiiAxQ74LwgGMX7a7knz8fmlachANDA5P/pCAemk0gCuOU43Y9ogCuEsaSP6kjE6Xi/LnQUf/tgdFqf2r2AO/1buU5R4oyOKnjcusktKmODiOenltrlf8Awt9jILDjUAQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/boilerplates/scorpion/root)
[![app-database](https://img.shields.io/static/v1?labelColor=362F2D&label=database&message=v1.0.0&color=362F2D&logo=data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8A////Hv///2L///+h////zf///+j////3/////v////7////3////6P///83///+h////Yv///x////8A////av///9L////6//////////////////////////////////////////////////////////r////S////av////f///////////////////////////////////////////////////////////////////////////////f//////////////+/////Q////sP///5b///+G////fv///37///+G////lv///7D////Q////7///////////////xP///3T///9B////Pf///07///9i////cv///3r///96////cv///2L///9O////Pf///0H///90////xf///y3///9v////vf///+b////4////////////////////////////////////+P///+b///+9////b////y3////F///////////////////////////////////////////////////////////////////////////////F////////////////////////////////////////////////////////////////////////////////////////////////////7////9D///+w////lv///4b///9+////fv///4b///+W////sP///9D////v///////////////E////dP///0H///89////Tv///2L///9y////ev///3r///9y////Yv///07///89////Qf///3T////F////Lf///2////+9////5v////j////////////////////////////////////4////5v///73///9v////Lf///8X//////////////////////////////////////////////////////////////////////////////8X/////////////////////////////////////////////////////////////////////////////////////////9///////////////////////////////////////////////////////////////////////////////9////2r////S////+v/////////////////////////////////////////////////////////6////0v///2r///8A////Hv///2L///+h////zf///+j////3/////v////7////3////6P///83///+h////Yv///x////8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==)](https://gitlab.com/hperchec/boilerplates/scorpion/database)
[![Docker](https://shields.io/static/v1?logo=docker&logoColor=white&label=&message=Docker&color=2496ED)](https://docker.com/)
[![MySQL](https://shields.io/static/v1?logo=mysql&logoColor=white&label=&labelColor=F29221&message=MySQL%205.7&color=3E6E93)](https://mysql.com/)
[![Discord](https://img.shields.io/discord/972997305739395092?label=Discord&logo=discord&logoColor=white)](https://discord.gg/AWTgVAVKaR)
[![pipeline status](https://gitlab.com/hperchec-boilerplates/scorpion/database/badges/main/pipeline.svg)](https://gitlab.com/hperchec/boilerplates/scorpion/database/commits/main)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[_TOC_]]

> **A LITTLE EXPLANATION**
>
> *Why not use Laravel database features?*
>
> I made the choice to not use Laravel database features for a simple reason. The goal is to make the database management totally independant of the Laravel framework.
> 
> In the future, if you migrate to another API server language/framework, you will not have to review the database management.
>
> So, as i am a nice guy, i have created a useful built-in tool to do exactly what Laravel *artisan* database related commands are capable 🤓.
>
> Have a look at the [documentation](#commands) below.

## Requirements

> **NOTE**: If you use Docker for this project, you don't need to install this dependencies in your local machine.

- MySQL 5.7 [https://dev.mysql.com/downloads/windows/installer/5.7.html](https://dev.mysql.com/downloads/windows/installer/5.7.html)

## Get started

> **IMPORTANT**: Please refer to the parent repository [documentation](https://gitlab.com/hperchec-boilerplates/scorpion/root.git).

If you want to clone this repository only:

```bash
git clone https://gitlab.com/hperchec/boilerplates/scorpion/database.git
```

By default, the database will be named **app**. Rename it as you want by editing the `./config` file:

```bash
# Database name
DATABASE_NAME="app"
```

## Commands

An all-in-one command is provided: `./cmd/cli.sh` (type `./cmd/cli.sh -h` to display help).

You can also display the help for each command by passing the `-h` option (`./cmd/cli.sh <command> -h`).

> **NOTE**: the `db` command is an alias for the `/home/cmd/cli.sh` bash script configured in the docker container (only available in the container).

### Create database

```bash
./cmd/cli.sh create
# OR (Docker)
db create
```

### Migrate

```bash
./cmd/cli.sh migrate [<name>]
# OR (Docker)
db migrate [<name>]
```

**Examples**:

```bash
./cmd/cli.sh migrate # Run all migrations
./cmd/cli.sh migrate 2022_04_28_17_00_00_create_users_table # Run a specific migration
# OR (Docker)
db migrate
db migrate 2022_04_28_17_00_00_create_users_table
```

### Seed

```bash
./cmd/cli.sh seed [<name>]
# OR (Docker)
db seed [<name>]
```

**Examples**:

```bash
./cmd/cli.sh seed # Run all seeders
./cmd/cli.sh seed users # Run a specific seeder
# OR (Docker)
db seed # Run all seeders
db seed users # Run a specific seeder
```

### Create migration file

```bash
./cmd/cli.sh make:migration <name> [-t <table>]
# OR (Docker)
db make:migration <name> [-t <table>]
```

> **NOTE**: the generated migration file will be automatically prefixed by the date in format `YYYY_MM_DD_hh_mm_ss_`.

**Example**:

```bash
./cmd/cli.sh make:migration create_users_table -t users
# OR (Docker)
db make:migration create_users_table -t users
# It will generate :
# - sql/migrations/2022_04_28_17_00_00_create_users_table.sql
```

### Create seeder file

```bash
./cmd/cli.sh make:seeder <table>
# OR (Docker)
db make:seeder <table>
```

**Example**:

```bash
./cmd/cli.sh make:seeder users
# OR (Docker)
db make:seeder users
# It will generate :
# - sql/seeders/users.sql
```

### Rollback migrations

```bash
./cmd/cli.sh migrate:rollback [<name>]
# OR (Docker)
db migrate:rollback [<name>]
```

**Examples**:

```bash
./cmd/cli.sh migrate:rollback # Run all migrations
./cmd/cli.sh migrate:rollback 2022_04_28_17_00_00_create_users_table # Run a specific migration
# OR (Docker)
db migrate:rollback # Run all migrations
db migrate:rollback 2022_04_28_17_00_00_create_users_table # Run a specific migration
```

### Refresh migrations

This command will rollback and re-run all migrations:

```bash
./cmd/cli.sh migrate:refresh
# OR (Docker)
db migrate:refresh
```

### Reset database

This command will drop entire database and re-create it.

```bash
./cmd/cli.sh reset [-y]
# OR (Docker)
db reset [-y]
```

> **NOTE**: the `-y` option disable the manual confirmation. **BE CAREFUL WITH IT!**

## Docker

### Environment file

Copy/paste the `.docker/.env.example` file and rename it to `.docker/.env`:

```bash
cp .docker/.env.example .docker/.env
```

### Build the image

```bash
docker build -t mysql-5.7 --target dev .
```

> **NOTE**: `mysql-5.7` will be the name of the image. You can modify it as you want.

### Run the container

This command will run the MySQL container named `mysql` based on the `mysql-5.7` image previously created, using the `.docker/.env` file.
The **working directory** `./sql` will be mapped to the **container volume** `/home/sql`.

Note that the **container port** `3306` will be related to the **local port** `3306`.

```bash
docker run -d -p 3306:3306 --env-file ./.docker/.env --name mysql --volume ./sql:/home/sql mysql-5.7
```

### Using GitLab

Login to gitlab registry:

```bash
docker login registry.gitlab.com
```

Build and push image to gitlab registry:

```bash
# Build the image (using 'prod' stage, see Dockerfile)
docker build --target prod -t registry.gitlab.com/hperchec/boilerplates/scorpion/database .
# Push the image to the container registry
docker push registry.gitlab.com/hperchec/boilerplates/scorpion/database
```

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)
